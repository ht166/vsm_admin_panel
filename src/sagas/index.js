import { all } from 'redux-saga/effects';
import login from '../pages/Login/Login.saga';


export default function* rootSaga() {
    yield all([
         login()
    ])
}