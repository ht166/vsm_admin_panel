
import React from "react";
import { connect } from 'react-redux';


// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Row,
  Col
} from "reactstrap";

import MicrosoftLogin from "react-microsoft-login";


class Login extends React.Component {

  constructor(props){
    super(props);
  }
  render() {
    return (
      <>
        <Col lg="5" md="7">
          <Card className="bg-secondary shadow border-0">
            <CardHeader className="bg-transparent pb-5">
              <div className="text-muted text-center mt-2 mb-3">
                <small>Sign in with</small>
              </div>
              <div className="btn-wrapper text-center">
                
              < MicrosoftLogin
                            clientId={'3f5acb71-d100-484d-9324-b9665aaee2c9'}
                            authCallback={(err, data) => {
                                                try {
                                                    console.log('...............Inital login value', data);
                                                    this.props.login(data);
                                                    const { accessToken } = this.props;
                                                    console.log('After Action Dispatched', accessToken);
                                                    
                                                    //to make Graph Calls
                                                    const urlBuf= "https://pixacoreindia.sharepoint.com/sites/tcp/_api/web//SiteUsers?$filter=substringof('" + data.userPrincipalName.toLowerCase() + "', LoginName)";
                                                    const url= 'https://graph.microsoft.com'+ "/v1.0/users/"+ data.userPrincipalName.toLowerCase() +'/memberOf';
                                                    // const url =  "https://cplace.sharepoint.com/sites/vsm/_api/web//SiteUsers?$filter=substringof('" + data.userPrincipalName.toLowerCase() + "', LoginName)";
                                                    const payload ={
                                                        method:'GET',
                                                        headers:{
                                                            
                                                            'Authorization': "Bearer " + accessToken,
                                                            'Access-Control-Allow-Origin': '*',
                                                            'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT',
                                                            
                                                        }
                                                    }

                                                    fetch(url,payload).then(res => res.json())
                                                    .then(res=> {
                                                        console.log('end of second calll..........',res)
                                                      });

                                                }
                                                catch{
                                                    console.log('error mai aaya', err)
                                                }
                                                        }

                            }
                            withUserData='true'
                            debug='true'
                        />
              </div>
            </CardHeader>
            {/* <CardBody className="px-lg-5 py-lg-5">
              <div className="text-center text-muted mb-4">
                <small>Or sign in with credentials</small>
              </div>
              <Form role="form">
                <FormGroup className="mb-3">
                  <InputGroup className="input-group-alternative">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="ni ni-email-83" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input placeholder="Email" type="email" />
                  </InputGroup>
                </FormGroup>
                <FormGroup>
                  <InputGroup className="input-group-alternative">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="ni ni-lock-circle-open" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input placeholder="Password" type="password" />
                  </InputGroup>
                </FormGroup>
                <div className="custom-control custom-control-alternative custom-checkbox">
                  <input
                    className="custom-control-input"
                    id=" customCheckLogin"
                    type="checkbox"
                  />
                  <label
                    className="custom-control-label"
                    htmlFor=" customCheckLogin"
                  >
                    <span className="text-muted">Remember me</span>
                  </label>
                </div>
                <div className="text-center">
                  <Button className="my-4" color="primary" type="button">
                    Sign in
                  </Button>
                </div>
              </Form>
            </CardBody> */}
          </Card>
          <Row className="mt-3">
            <Col xs="6">
              <a
                className="text-light"
                href="#pablo"
                onClick={e => e.preventDefault()}
              >
                <small>Forgot password?</small>
              </a>
            </Col>
            <Col className="text-right" xs="6">
              <a
                className="text-light"
                href="#pablo"
                onClick={e => e.preventDefault()}
              >
                <small>Account Create NEw</small>
              </a>
            </Col>
          </Row>
        </Col>
      </>
    );
  }
}


const mapStateToProps = (state) => {
  return {
      accessToken: state.LoginReducer.accessToken
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
      login: (data) => dispatch({ type: 'GET_TOKEN', data }),

  }
}



export default connect(mapStateToProps, mapDispatchToProps)(Login);
