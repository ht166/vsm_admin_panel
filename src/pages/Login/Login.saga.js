import { takeEvery, put, takeLatest } from 'redux-saga/effects';


function* getTokenAsync({data}) {
    console.log('thisis inside the saga file', data);
    yield put({ type: 'GET_TOKEN_ASYNC' , data})
}


function* login() {
    yield takeLatest('GET_TOKEN', getTokenAsync);
}


export default login;