const initState = {
    signInstatus: false,
    accessToken: ''
}

const LoginReducer = (state = initState, action) => {
    const { type, data } = action;
    switch (type) {
        case 'GET_TOKEN_ASYNC':
            console.log('getting inside async call', data);
            const { accessToken } = data;
            return { ...state, accessToken };

        default: return state;
    }
}

export default LoginReducer;
