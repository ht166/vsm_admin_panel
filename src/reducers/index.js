import { combineReducers } from 'redux';
import LoginReducer from '../pages/Login/Login.reducer';
export const rootReducer = combineReducers(
    {
        LoginReducer,
    }
)
